## **3 Scenario description**

The gift giving process under 3 Scenario provides for:

1. the user who will act as the Giver is the user of Developer’s app;
2. the user who will act as the Presentee may not be a user of Developer’s app;
3. on Developer’s application side there is **NO** information about:
	- the country of residence of users who act as the Giver and the Presentee;
	- the city of residence of users who act as the Giver and the Presentee;
4. after the initialization of the 3 Scenario, the Giver will be able to continue the gift giving process using the branch of the:
 	- 3a Scenario
 	- or 3b Scenario
 	- or 3c Scenario
  
### **3 Scenario initialization**

1. Send the POST request to `https://service.yougiver.me/api/v1/gift_requests` with the possible parameters:

<!--table-->

| Parameter                               | Description                                  |
|---------------------------------------  |------------------------------------------ |
| gift_request[developer_number] **\***   | Your developer_number in Yougiver service   |
| gift_request[giver_id] **\***           | Giver's ID in your service    |
| gift_request[giver_name]                | Giver's Name                              |
| gift_request[giver_email]               | Giver's Email                            |
| gift_request[giver_phone]               | Giver's Phone number                          |
| gift_request[giver_nickname]            | Giver's Nickname                          |
| gift_request[giver_gender]              | Giver's Gender                              |
<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Parameter | Description                                      |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->
